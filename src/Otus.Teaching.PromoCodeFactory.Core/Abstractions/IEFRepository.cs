﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface IEFRepository <T> where T : BaseEntity
    {
        void Create(T entity);
        T FindById (Guid id);
        Task<T> AsyncFindById(Guid id);
        IEnumerable<T> Get();
        void Update(T entity);
        void Delete(T entity);
    }
}
