﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        //public Guid RoleId { get; set; }
        //public Role Roles { get; set; }
        public Guid PreferenceId { get; set; }
        public Preference Preferences { get; set; }
    }
}