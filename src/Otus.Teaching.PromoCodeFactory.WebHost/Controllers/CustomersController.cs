﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IEFRepository<Customer> _customerRepository;
        public CustomersController(IEFRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }
        [HttpGet]
        public List<CustomerShortResponse> GetCustomers()
        {
            var res = _customerRepository.Get();
            List<CustomerShortResponse> resu = new List<CustomerShortResponse>();

            foreach (var item in res)
            {
                resu.Add(new CustomerShortResponse()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Email = item.Email
                });
            }

            return resu;
        }
        
        [HttpGet("{id}")]
        public async Task<CustomerShortResponse> GetCustomerAsync(Guid id)
        {
            var res = await _customerRepository.AsyncFindById(id);
            if (res != null)
            {
                var resu = new CustomerShortResponse()
                {
                    Id = res.Id,
                    FirstName = res.FirstName,
                    LastName = res.LastName,
                    Email = res.Email
                };
                return resu;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public Guid CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var item = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PreferenceId = request.PreferenceIds.First(),
            };
            _customerRepository.Create(item);
            return item.Id;
        }
        
        [HttpPut("{id}")]
        public IActionResult EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var item = GetCustomerAsync(id).Result;
            if (item != null)
            {
                if (item.Id != Guid.Empty)
                {
                    var i = new Customer()
                    {
                        Id = item.Id,
                        Email = request.Email,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        PreferenceId = request.PreferenceIds.First(),
                    };
                    _customerRepository.Update(i);
                }
                return StatusCode(StatusCodes.Status200OK);
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }
        }
        
        [HttpDelete]
        public IActionResult DeleteCustomer(Guid id)
        {
            var item = GetCustomerAsync(id).Result;
            if (item != null)
            {
                if (item.Id != Guid.Empty)
                {
                    var i = new Customer()
                    {
                        Id = item.Id,
                        Email = item.Email,
                        FirstName = item.FirstName,
                        LastName = item.LastName
                    };
                    _customerRepository.Delete(i);
                }
                return StatusCode(StatusCodes.Status200OK);
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }
        }
    }
}