﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.AppDbContext;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFRepository<T> : IEFRepository<T> where T : BaseEntity
    {
        private readonly LiteDbContext _context;
        internal DbSet<T> _dbSet;
        public EFRepository(LiteDbContext context)
        {
            _context = context;
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _dbSet = context.Set<T>();
        }
        public IEnumerable<T> Get()
        {
            return  _dbSet.AsNoTracking().ToList();
        }
        public void Create(T entity)
        {
            _context.Add<T>(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            var local = _context.Set<T>()
                                .Local
                                .FirstOrDefault(entry => entry.Id.Equals(entity.Id));
            if (local != null)
            {
                _context.Entry(local).State = EntityState.Detached;
            }
            _context.Entry(entity).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public T FindById(Guid id)
        {            
            return _context.Find<T>(id);
        }
        public async Task<T> AsyncFindById(Guid id)
        {
            return await _context.FindAsync<T>(id); 
        }
        public void Update(T entity)
        {
            var local = _context.Set<T>()
                                .Local
                                .FirstOrDefault(entry => entry.Id.Equals(entity.Id));
            if (local != null)
            {
                _context.Entry(local).State = EntityState.Detached;
            }
            _context.Entry(entity).State = EntityState.Modified;
           _context.SaveChanges();
        }

    }
}
