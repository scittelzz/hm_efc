﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddedAddressCustomer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("06a9b0c3-dd3c-4206-84fe-68fc18d93e66"));

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("5cc849f5-443e-4f08-9eed-741c2862ebe4"));

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("721c847b-1273-42e7-b6f9-42d289739e50"));

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Customer",
                type: "TEXT",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "Id", "Address", "Email", "FirstName", "LastName" },
                values: new object[,]
                {
                    { new Guid("39aebbbf-871d-45d7-af1f-cd90e6584049"), null, "ivan_petrov@mail.ru", "Иван", "Петров" },
                    { new Guid("8cf01c3f-2308-432b-96fd-e9ba6a476da2"), null, "s_sergeev@mail.ru", "Степан", "Сергеев" },
                    { new Guid("f887ce19-2cef-42b0-8029-19fc22f8244f"), null, "ivan_kolosov@mail.ru", "Иван", "Колосов" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("39aebbbf-871d-45d7-af1f-cd90e6584049"));

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("8cf01c3f-2308-432b-96fd-e9ba6a476da2"));

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: new Guid("f887ce19-2cef-42b0-8029-19fc22f8244f"));

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Customer");

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[,]
                {
                    { new Guid("06a9b0c3-dd3c-4206-84fe-68fc18d93e66"), "ivan_kolosov@mail.ru", "Иван", "Колосов" },
                    { new Guid("5cc849f5-443e-4f08-9eed-741c2862ebe4"), "ivan_petrov@mail.ru", "Иван", "Петров" },
                    { new Guid("721c847b-1273-42e7-b6f9-42d289739e50"), "s_sergeev@mail.ru", "Степан", "Сергеев" }
                });
        }
    }
}
