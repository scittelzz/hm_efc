﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.AppDbContext;

public class LiteDbContext : DbContext
{
    public IConfiguration _config { get; set; }
    public DbSet<Employee> Employee { get; set; }
    public DbSet<Role> Role { get; set; }
    public DbSet<Customer> Customer { get; set; }
    public DbSet<Preference> Preference { get; set; }
    public DbSet<PromoCode> PromoCode { get; set; }
    public LiteDbContext(IConfiguration config) : base()
    {
        _config = config;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(_config.GetConnectionString("DatabaseConnection"));
        optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(50);
        modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(50);
        modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(50);
        modelBuilder.Entity<Employee>().HasKey(c => c.Id);
        modelBuilder.Entity<Employee>().HasOne(r => r.Role).WithMany().HasForeignKey(ri => ri.RoleId).IsRequired();

        modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(80);

        modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(40);
        modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(100);
        modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(80);
        modelBuilder.Entity<PromoCode>().HasKey(c => c.Id);

        modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(50);
        modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(50);
        modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(50);
        modelBuilder.Entity<Customer>().HasKey(c => c.Id);
        modelBuilder.Entity<Customer>().HasOne(p => p.Preferences).WithMany().HasForeignKey(pid => pid.PreferenceId).IsRequired();

        modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(50);
        modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(50);
        modelBuilder.Entity<Role>().HasKey(c => c.Id);

        modelBuilder.Entity<PromoCode>()
            .HasOne(i => i.Preference);

        modelBuilder.Entity<Role>().HasData(
            FakeDataFactory.Roles
            );
        modelBuilder.Entity<Preference>().HasData(
            FakeDataFactory.Preferences
            );
        modelBuilder.Entity<Customer>().HasData(
            FakeDataFactory.Customers
            );
        modelBuilder.Entity<Employee>().HasData(
            FakeDataFactory.Employees
            );
    }

}
